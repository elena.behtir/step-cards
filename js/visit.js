// import { visit } from "./cards.js"
// import { api } from "./api.js"
import { modal } from "./modal.js"

class Visit {
    constructor(id, fullname, doctor, purpose, description, urgency, completed) {
        this.id = id;
        this.fullname = fullname;
        this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.completed = completed;
    }

    renderCard() {
        const cards = document.querySelector('.cards');
        const newCard = document.createElement('div');
        newCard.classList.add('new-card');
        cards.append(newCard);
        newCard.innerText = `${this.fullname}, ${this.purpose}, ${this.description}`;

    }
}

class CreateVisit {
    emptyField() {
        const errorForm = document.createElement('div');
        errorForm.innerText = 'Заповніть усі поля форми!';
        document.body.append(errorForm);
    }


    renderModalVisit() {
        const wrapperVisit = document.createElement('div');
        document.body.appendChild(wrapperVisit);
        wrapperVisit.classList.add('wrap-visit');
        const modalNewVisit = document.createElement('div');
        modalNewVisit.classList.add('modal-newvisit');
        wrapperVisit.appendChild(modalNewVisit);
        document.body.style.height = '800px';
        wrapperVisit.addEventListener('click', (e) => {
            if (!(e.target.closest('.modal-newvisit'))) {
                wrapperVisit.remove();
                document.body.style.height = '100vh';
            };
        });

        const inputFullname = document.createElement('div');
        inputFullname.innerHTML = '<label class="name-input">ПІБ пацієнта<input class="input-visit input-fullname" type = "text" required /></label>';

        const inputPuspose = document.createElement('div');
        inputPuspose.innerHTML = '<label class="name-input">Мета візиту<input class="input-visit input-purpose" type = "text" required /></label>';
        const inputDescription = document.createElement('div');
        inputDescription.innerHTML = '<label class="name-input">Стислий опис візиту<textarea class="input-visit input-description" required></textarea></label>';

        const wrapSelect = document.createElement('div');
        wrapSelect.classList.add('wrap-select-modalvisit');

        const selectDoctor = document.createElement('div');
        selectDoctor.innerHTML = '<select class="select select-doctor" name="" id="doctors"><option value="doc">оберіть лікаря</option><option class="dentist" value="dentist">Стоматолог</option><option class="cardiologis" value="cardiologis">Кардіолог</option><option class="therapist" value="therapist">Терапевт</option></select>';

        const addField = document.createElement('div');
        addField.classList.add('add-field');

        const selectUrgency = document.createElement('div');
        selectUrgency.innerHTML = '<select class="select select-urgency" name="" id="urgency"><option>терміновість візиту</option><option class="high-urgency" value="high">терміново</option><option class="normal-urgency" value="normal">звичайний</option><option class="low-urgency" value="low">плановий</option></select>';

        const selectDone = document.createElement('div');
        selectDone.innerHTML = '<select class="select select-done" name="" id="status"><option>статус візиту</option><option class="visit-open" value="open">візит очікується</option><option class="visit-done" value="done">візит відбувся</option></select>';

        const inputAddComment = document.createElement('div');
        inputAddComment.innerHTML = '<label class="name-input">Додаткові коментарі<textarea class="input-visit input-addcomment"></textarea></label>';

        const btnWrapper = document.createElement('div');
        btnWrapper.classList.add('btn-wrapper');
        const btnPostVisit = document.createElement('button');
        btnPostVisit.innerText = 'Створити';
        btnPostVisit.classList.add('btn-visit');
        btnPostVisit.classList.add('create-card');
        btnPostVisit.setAttribute('type', 'submit');

        const btnCloseVisit = document.createElement('button');
        btnCloseVisit.innerText = 'Закрити';
        btnCloseVisit.classList.add('btn-visit');
        btnCloseVisit.classList.add('close-visit');
        btnCloseVisit.addEventListener('click', (e) => {
            wrapperVisit.remove();
        });

        modalNewVisit.append(inputFullname, inputPuspose, inputDescription, wrapSelect, addField, inputAddComment, btnWrapper);
        wrapSelect.append(selectDoctor, selectUrgency, selectDone);
        btnWrapper.append(btnPostVisit, btnCloseVisit);

        const doctors = document.querySelector("#doctors");
        doctors.addEventListener('change', (e) => {
            console.log(e.target.value);
            let doctor = e.target.value;
            addField.innerHTML = "";
            if (doctor === 'dentist') {
                console.log("add dentist visit");
                const inpLastVisit = document.createElement('div');
                inpLastVisit.innerHTML = '<label class="name-input"><i class="fa-solid fa-tooth"></i> Дата останнього візиту<input class="input-visit input-lastvisit" type = "text" required /></label>';
                addField.appendChild(inpLastVisit);
            } else {
                if (doctor === 'cardiologis') {
                    console.log("add cardiologist visit");
                    const inpPressure = document.createElement('div');
                    inpPressure.innerHTML = '<label class="name-input"><i class="fa-solid fa-heart"></i>Робочий тиск<input class="input-visit input-pressure" type = "text" required /></label>';
                    const inpAge = document.createElement('div');
                    inpAge.innerHTML = '<label class="name-input">Вік<input class="input-visit input-age" type = "text" required /></label>';
                    const inpWeight = document.createElement('div');
                    inpWeight.innerHTML = '<label class="name-input">Індекс маси тіла<input class="input-visit input-weight" type = "text" required /></label>';
                    const inpIllness = document.createElement('div');
                    inpIllness.innerHTML = '<label class="name-input">Перенесені захворювання серцево-судинної системи<textarea class="input-visit input-illness" required></textarea></label>';

                    addField.append(inpPressure, inpAge, inpWeight, inpIllness);

                } else {
                    if (doctor === 'therapist') {
                        const inpAge = document.createElement('div');
                        inpAge.innerHTML = '<label class="name-input"><i class="fa-solid fa-stethoscope"></i>Вік<input class="input-visit input-age" type = "text" required /></label>';
                        addField.appendChild(inpAge);
                    }
                }
            }
        })

        btnPostVisit.addEventListener('click', (e) => {
            const fullname = document.querySelector('.input-fullname').value;
            const doctor = document.querySelector('.select-doctor').value;
            const purpose = document.querySelector('.input-purpose').value;
            const description = document.querySelector('.input-description').value;
            const urgency = document.querySelector('.select-urgency').value;
            const completed = document.querySelector('.select-done').value;

            if (fullname === '' || doctor === '' || purpose === '' || description === '' || urgency === '' || completed === '') {
                this.emptyField();

            } else if (doctor === 'dentist') {
                const lastvisit = document.querySelector('.input-lastvisit').value;
                if (lastvisit === '') {
                    this.emptyField();
                } else {
                    this.createPost(fullname, doctor, purpose, description, urgency, completed);
                }

            } else if (doctor === 'cardiologis') {
                const pressure = document.querySelector('.input-pressure').value;
                const age = document.querySelector('.input-age').value;
                const weight = document.querySelector('.input-weight').value;
                const illness = document.querySelector('.input-illness').value;
                if (pressure === '' || age === '' || weight === '' || illness === '') {
                    this.emptyField();
                } else {
                    this.createPost(fullname, doctor, purpose, description, urgency, completed);
                }

            } else if (doctor === 'therapist') {
                const age = document.querySelector('.input-age').value;
                if (age === '') {
                    this.emptyField();
                } else {
                    this.createPost(fullname, doctor, purpose, description, urgency, completed);
                }


            } else {
                this.createPost(fullname, doctor, purpose, description, urgency, completed);
            }

        })
    }

    createPost(fullname, doctor, purpose, description, urgency, completed) {
        console.log(fullname, doctor, purpose, description, urgency, completed);
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                fullname: `${fullname}`,
                doctor: `${doctor}`,
                purpose: `${purpose}`,
                description: `${description}`,
                urgency: `${urgency}`,
                completed: `${completed}`
            })
        })
            .then(response => response.json())
            .then(data => {
                console.log(data.id);
                const newId = data.id;
                fetch(`https://ajax.test-danit.com/api/v2/cards/${newId}`, {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`,
                    },
                })
                    .then((r) => r.json())
                    .then((d) => {
                        console.log(d);
                        const cards = document.querySelector('.cards');
                        const newCard = document.createElement('div');
                        newCard.classList.add('new-card');
                        cards.append(newCard);

                        newCard.innerText = `${d.fullname}, ${d.purpose}, ${d.description}`;
                        const modalNewVisit = document.querySelector('.modal-newvisit');
                        modalNewVisit.remove();
                        modal.deleteFirstInfo();
                    });
            })
    }

    createPut(fullname, doctor, purpose, description, urgency, completed) {
        console.log(fullname, doctor, purpose, description, urgency, completed);
        fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                fullname: `${fullname}`,
                doctor: `${doctor}`,
                purpose: `${purpose}`,
                description: `${description}`,
                urgency: `${urgency}`,
                completed: `${completed}`
            })
        })
            .then(response => response.json())
            .then(response => console.log(response))
    }
}

const createVisit = new CreateVisit();

export { createVisit };