// тестовий запит
// const token = '5898ca5b-ff35-42eb-abbb-b8faf8f084bb'

// fetch("https://ajax.test-danit.com/api/v2/cards", {
//   method: 'POST',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${token}`
//   },
//   body: JSON.stringify({
//     title: 'Визит к кардиологу',
//     description: 'Плановый визит',
//     doctor: 'Cardiologist',
//     bp: '24',
//     age: 23,
//     weight: 70
//   })
// })
//   .then(response => response.json())
//   .then(response => console.log(response))

// const email = 'elena.behtir@gmail.com';
// const pass = '09051977arsenal';
// async function init() {
//   if (localStorage.getItem('token')) {
//     console.log('logined');
//     // await getCards();
//   } else {
//     await login();
//     // await getCards();
//   }
// }

// email = 'elena.behtir@gmail.com'
// password = '09051977arsenal'

//   fetch('https://ajax.test-danit.com/api/v2/cards/login', {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify({ email: email, password: password }),
//   })
//     .then((response) => response.text())
//     .then((token) => {
//       console.log(token);
//       localStorage.setItem('token', token);
//     });

const cardsContainer = document.querySelector('.cards');

function getCards() {
  return fetch('https://ajax.test-danit.com/api/v2/cards', {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  })
    .then((r) => r.json())
    .then((d) => {
      if (d.length) {
        d.forEach((visit) => {
          const card = document.createElement('div');
          card.classList.add('card');

          const cardHeader = document.createElement('div');
          cardHeader.classList.add('card-header');

          const name = document.createElement('p');
          name.innerHTML = `ФИО: ${visit.fullname}`;
          cardHeader.appendChild(name);

          const doctor = document.createElement('p');
          doctor.innerHTML = `Врач: ${visit.doctor}`;
          cardHeader.appendChild(doctor);

          const showMore = document.createElement('button');
          showMore.innerHTML = 'Показать больше';
          cardHeader.appendChild(showMore);

          const edit = document.createElement('button');
          edit.innerHTML = 'Редактировать';
          cardHeader.appendChild(edit);

          const remove = document.createElement('button');
          remove.innerHTML = 'х';
          cardHeader.appendChild(remove);

          const cardBody = document.createElement('div');
          cardBody.classList.add('card-body');

          const purpose = document.createElement('p');
          purpose.innerHTML = `Цель визита: ${visit.purpose}`;
          cardBody.appendChild(purpose);

          const description = document.createElement('p');
          description.innerHTML = `Краткое описание: ${visit.description}`;
          cardBody.appendChild(description);

          const urgency = document.createElement('p');
          urgency.innerHTML = `Срочность: ${visit.urgency}`;
          cardBody.appendChild(urgency);

          if (visit.lastvisit) {
            const lastvisit = document.createElement('p');
            lastvisit.innerHTML = `Дата последнего визита: ${visit.lastvisit}`;
            cardBody.appendChild(lastvisit);
          }

          if (visit.age) {
            const age = document.createElement('p');
            age.innerHTML = `Возраст: ${visit.age}`;
            cardBody.appendChild(age);
          }

          if (visit.illness) {
            const illness = document.createElement('p');
            illness.innerHTML = `Болезнь: ${visit.illness}`;
            cardBody.appendChild(illness);
          }

          if (visit.pressure) {
            const pressure = document.createElement('p');
            pressure.innerHTML = `Болезнь: ${visit.pressure}`;
            cardBody.appendChild(pressure);
          }

          if (visit.weight) {
            const weight = document.createElement('p');
            weight.innerHTML = `Вес: ${visit.weight}`;
            cardBody.appendChild(weight);
          }

          const status = document.createElement('p');
          status.innerHTML = `Статус: ${visit.completed}`;
          cardBody.appendChild(status);

          showMore.addEventListener('click', () => {
            if (card.classList.contains('open')) {
              showMore.innerHTML = 'Показать больше';
              card.classList.remove('open');
            } else {
              showMore.innerHTML = 'Показать меньше';
              card.classList.add('open');
            }
          })

          remove.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/v2/cards/${visit.id}`, {
              method: 'DELETE',
              headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              },
            })
            card.remove();
          })

          card.appendChild(cardHeader);
          card.appendChild(cardBody);
          cardsContainer.appendChild(card);
        });
      } else {
        cardsContainer.innerHTML = `<div class="first-vizit">Жодного елемента ще не додано</div>`;
      }
      console.log(d);
    })
    .catch(() => {
      cardsContainer.innerHTML = `<div class="first-vizit">Жодного елемента ще не додано</div>`;
    });
}

getCards();